 #Latvijas administratīvais iedalījums

 - Republikas pilsētas
 - Novadi
 - Pagasti

**Informācijas avots**: [Administratīvo teritoriju un apdzīvoto vietu likums](http://www.likumi.lv/doc.php?id=185993) (http://www.likumi.lv/doc.php?id=185993)

**Aktualitāte**: Likuma grozījumi, kas stājas spēkā 03.08.2011

